import React, { Component } from "react";

export default class Search extends Component {
  render() {
    var unitChoice = this.props.imperialUnits ? (
      <div className="unitChoice" onClick={this.props.changeUnit}>
          <span className="unitChoice__unit">
            &#8457;
          </span>
          <img className="unitChoice__arrow" src="../../assets/icons/rightArrow.png" />
        </div>) : (
        <div className="unitChoice" onClick={this.props.changeUnit}>
          <img className="unitChoice__arrow" src="../../assets/icons/leftArrow.png" />
          <span className="unitChoice__unit">
            &#8451;
          </span>
        </div>);
    return (
      <div className="search">
        <input className="search__searchBar" onChange={this.props.onChange} id="geocomplete" type="text" placeholder="Search Locations" onKeyDown={this.props.onPlaceSearch} />
        <input className="search__geolocateButton" onClick={this.props.showPermission} type="submit" value="Geolocate" />
        {unitChoice}
      </div>
    );
  }
}