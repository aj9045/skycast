import React, { Component } from "react";
import Weekday from "./Weekday";

export default class WeatherWeek extends Component {
  render() {
    var weather = this.props.weather.data;

    var week = weather.map((day, index) => {
      return (
        <Weekday key={index} index={index} day={day} imperialUnits={this.props.imperialUnits} showMoreDetail={this.props.showMoreDetail} detailIndex={this.props.detailIndex} />
      );
    })
    return (
      <div className="weatherWeek">
        {week}
      </div>
    );
  }
}