import React, { Component } from "react";
import HistoryItem from "./HistoryItem";

export default class HistoryList extends Component {
  render() {
    var history = this.props.history;
    var historyItems = history.map((item, index) => {
      return (
        <HistoryItem key={index} index={index} item={item} onHistoryItemClick={this.props.onHistoryItemClick} />
      );
    })

    return (
      <div className="historyList">
        {historyItems}
      </div>
    );
  }
}