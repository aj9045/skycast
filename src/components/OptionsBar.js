import React, { Component } from "react";

export default class OptionsBar extends Component {
  render () {
    return (
      <div className="optionsBar">
        <div className="optionsBar__weather" onClick={this.props.onOptionSelect} value="current">Weather</div>
        <div className="optionsBar__searched" onClick={this.props.onOptionSelect} value="searched">Recently searched</div>
      </div>
);
  }
}