import React, { Component } from "react";

export default class WeatherToday extends Component {
  render() {
    var weather = this.props.weather;
    var moreToday = this.props.moreToday;
    var unit = this.props.imperialUnits ? "mph" : "kph";
    return (
      <div className="currentWeather__more">
        <div className="currentWeather__more--top">
          <p className="currentWeather__more__precip"><span className="bold">Precip.</span> {Math.round(weather.precipProbability * 100)}% <span className="keyword">{weather.precipType}</span></p>
          <p className="currentWeather__more__humidity"><span className="bold">Humidity</span> {Math.round(weather.humidity * 100)}%</p>
          <p className="currentWeather__more__wind"><span className="bold">Wind</span> {Math.round(weather.windSpeed)} {unit}</p>
        </div>
        <div className="currentWeather__more--center">
          <h2 className="bold currentWeather__more__title">Today</h2>
          <div className="currentWeather__historicData__button" onClick={this.props.getTimestampHistory}>View Historic Data</div>
        </div>
        <div className="currentWeather__more--bottom">
          <p className="currentWeather__more__high"><span className="bold">High</span> of <span className="keywordHigh">{Math.round(moreToday.temperatureMax)}&#176;</span></p>
          <p className="currentWeather__more__low"><span className="bold">Low</span> of <span className="keywordLow">{Math.round(moreToday.temperatureMin)}&#176;</span></p>
        </div>

      </div>
    );
  }
}