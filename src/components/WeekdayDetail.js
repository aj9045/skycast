import React, { Component } from "react";

export default class Weekday extends Component {
  render() {
    var day = this.props.day;
    return (
      <div className="weekdayDetail">
      <div className="weekdayDetail__likeTemps">
          <p className="weekdayDetail__likeTemps--high">
            <span className="bold">High</span> feels like <span className="keyword">{Math.round(day.apparentTemperatureMax)}&#176;</span>
          </p>
          <p className="weekdayDetail__likeTemps--low">
            <span className="bold">Low</span> feels like <span className="keyword">{Math.round(day.apparentTemperatureMin)}&#176;</span>
          </p>
        </div>

        <div className="weekdayDetail__summary">
          {day.summary}
        </div>
        <div className="weekdayDetail__precip">
          <span className="bold">Chance of Precip.</span> {Math.round(day.precipProbability * 100)}% <span className="keyword">{day.precipType}</span>
        </div>

        <div className="weekdayDetail__more">
          <p className="weekdayDetail__more__data">
            <span className="bold">Humidity</span> {Math.round(day.humidity * 100)}%
          </p>
          <p className="weekdayDetail__more__data">
            <span className="bold">Wind</span> {Math.round(day.windSpeed)} {this.props.unit}
          </p>
        </div>

      </div>
    );
  }
}