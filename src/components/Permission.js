import React, { Component } from "react";

export default class Permission extends Component {
  render() {
    return (
      <div className="permission">
        <p className="permission__question">May we fetch your coordinates?</p>
        <input className="input permission__yes" type="submit" value="Yes" onClick={this.props.geolocate} />
        <input className="input permission__no" type="submit" value="No" onClick={this.props.denyGeolocation} />
      </div>
    );
  }
}