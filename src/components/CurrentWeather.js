import React, { Component } from "react";
import CurrentWeatherNow from "./CurrentWeatherNow";
import WeatherToday from "./WeatherToday";
import WeatherWeek  from "./WeatherWeek";

export default class CurrentWeather extends Component {
  render () {
    var weather = this.props.currentWeather;
    var icons = ["clear-day", "clear-night", "partly-cloudy-day", "partly-cloudy-night", "cloudy", "fog", "snow", "wind", "rain", "thunderstorm", "sleet"];
    var iconSrc;
    $.each(icons, function(index) {
      if (weather.currently.icon === icons[index]) {
        iconSrc = "./assets/icons/" + icons[index] + ".png"
        return false;
      }
      else {iconSrc = "./assets/icons/default.png";}
      });
    var moreToday = this.props.currentWeather.daily.data[0];
    var week = this.props.showWeek ? <WeatherWeek weather={weather.daily} imperialUnits={this.props.imperialUnits} showMoreDetail={this.props.showMoreDetail} detailIndex={this.props.detailIndex} /> : <div />;
    var showWeekButton = this.props.showWeek ? <div className="showWeek" onClick={this.props.onShowWeek}><img className="showWeek__img" src="./assets/icons/lessArrow.png" /></div> : <div className="showWeek" onClick={this.props.onShowWeek}><img className="showWeek__img" src="./assets/icons/moreArrow.png" /></div>
    return (
      <div className="currentWeather">
        <h1 className="currentWeather__place">{this.props.address}</h1>
        <CurrentWeatherNow weather={this.props.currentWeather} iconSrc={iconSrc} />
        <WeatherToday weather={weather.currently} moreToday={moreToday} imperialUnits={this.props.imperialUnits} getTimestampHistory={this.props.getTimestampHistory} />
        <div className="graph"></div>
        {showWeekButton}
        {week}
      </div>
    );
  }
}