import React, { Component } from "react";

export default class CurrentWeatherNow extends Component {
  render() {
    var weather = this.props.weather.currently;
    var iconSrc = this.props.iconSrc;
    return (
      <div className="currentWeather__now">
        <div className="currentWeather__now--top">
          <div>
            <img className="currentWeather__now__icon" src={iconSrc} />
          </div>
          <p className="currentWeather__now__temp keyword">{Math.round(weather.temperature)}&#176;</p>
        </div>
        <div className="currentWeather__now--bottom">
          <p className="currentWeather__now__summary">{weather.summary}</p>
          <p className="currentWeather__now__likeTemp">Feels Like <span className="keyword">{Math.round(weather.apparentTemperature)}&#176;</span></p>
        </div>
      </div>
    );
  }
}