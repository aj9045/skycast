import React, { Component } from "react";
import WeekdayDetail from "./WeekdayDetail";

export default class Weekday extends Component {
  getReadableDate(time) {
    var monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    var date = new Date(time * 1000);
    var month = monthNames[date.getMonth() + 1];
    var day = date.getDate();
    return month + " " + day;
  }

  getDayName(time) {
    var days = ["Sunday" ,"Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
    var date = new Date(time * 1000);
    return days[date.getDay()];
  }

  render() {
    var day = this.props.day;
    var date = this.getReadableDate(day.time);
    var dayName = this.getDayName(day.time);
    var unit = this.props.imperialUnits ? "mph" : "kph";
    var icons = ["clear-day", "clear-night", "partly-cloudy-day", "partly-cloudy-night", "cloudy", "fog", "snow", "wind", "rain", "thunderstorm", "sleet"];
    var iconSrc;
    $.each(icons, function(index) {
      if (day.icon === icons[index]) {
        iconSrc = "./assets/icons/" + icons[index] + ".png"
        return false;
      }
      else {iconSrc = "./assets/icons/default.png";}
      });
    var detail = this.props.detailIndex === this.props.index ? <WeekdayDetail day={day} date={date} dayName={dayName} iconSrc={iconSrc} unit={unit} /> : <div />;
    return (
      <div className="weekday">
        <div className="weekday__bit" onClick={this.props.showMoreDetail} value={this.props.index}>
          <div className="weekday__bit__left">
            <p className="weekday__bit__left__day bold">{dayName}</p>
            <p className="weekday__bit__left__date">{date}</p>
          </div>
          <div className="weekday__bit__center">
            <img className="weekday__bit__center--img" src={iconSrc} />
          </div>
          <div className="weekday__bit__right">
            <p className="weekday__bit__right--temps"><span className="keywordHigh">{Math.round(day.temperatureMax)}&#176;</span> / <span className="keywordLow">{Math.round(day.temperatureMin)}&#176;</span></p>
          </div>
        </div>
        {detail}
      </div>
    );
  }
}