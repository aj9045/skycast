import React, { Component } from "react";

export default class HistoryItem extends Component {
  render() {
    var item = this.props.item;
    return (
      <div className="historyItem" onClick={this.props.onHistoryItemClick} value={item.place}>
          <span className="historyItem__place" value={item.place}>
            {item.place}
          </span>
      </div>
    );
  }
}