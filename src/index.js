import React, { Component } from "react";
import ReactDOM from "react-dom";
import SkyLight from "react-skylight";
import Search from "./components/Search";
import Permission from "./components/Permission";
import CurrentWeather from "./components/CurrentWeather";
import HistoryList from "./components/HistoryList";
import OptionsBar from "./components/OptionsBar";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      initial: true,
      coords: [],
      geolocate: null,
      input: "New York, NY, USA",
      weather: {},
      history: [],
      imperialUnits: true,
      unitChange: false,
      selected: "current",
      loadingLocation: false,
      showWeek: false,
      detailIndex: "",
      timestampHistory: [],
      historicOpen: false
    }
  }

  componentWillMount() {
    this._getCoordinates(this.state.input);
  }

  _changeUnit() {
    this.setState({
      imperialUnits: !this.state.imperialUnits, unitChange: true},
      function() {
        if (this.state.historicOpen) {
          this._getTimestampHistory();
        }
        this._findWeather(this.state.coords);
      }.bind(this)
    );
  }

  _showPermission() {
    if (!this.state.geolocate) {
      this.refs.modal.show();
    }
    else {
      this._geolocate();
    }
  }

  _denyGeolocation() {
    this.refs.modal.hide();
    this.setState({geolocate: false});
  }

  _geolocate() {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(this._getGeolocateCoords.bind(this));
      this.setState({geolocate: true, loadingLocation: true});
    } else {
      this.setState({geolocate: false});
      alert("Geo Location is not supported");
    }
  }

  _getGeolocateCoords(position) {
    var lat = position.coords.latitude;
    var lon = position.coords.longitude;
    var latlon = new google.maps.LatLng(lat, lon);
    var geocoder = new google.maps.Geocoder();
    geocoder.geocode({ "latLng": latlon }, function (results, status) {
      if (status == google.maps.GeocoderStatus.OK) {
        var addressComponents = results[0].address_components;
        for (var i=0; i < addressComponents.length; i++) {
          if (addressComponents[i].types[0] === "neighborhood") {
              var city = addressComponents[i].long_name;
          }
          if (addressComponents[i].types[0] === "administrative_area_level_1") {
              var region = addressComponents[i].long_name;
          }
          if (addressComponents[i].types[0] === "country") {
              var country = addressComponents[i].long_name;
          }
        }

        var address = city + ", " + region + ", " + country;
        this.setState({coords: [lat, lon], input: address, selected: "current"});
        this._findWeather([lat,lon]);
      }
      else {
        alert(status);
      }
    }.bind(this));
  }

  _onChangeSearchText(e) {
    new google.maps.places.Autocomplete(
      (document.getElementById("geocomplete")),
      {types: ["(cities)"]});
  }

  _onPlaceSearch(e) {
    if (e.keyCode === 13) {
      var input = e.target.value;
      this.state.selected === "current" ? this.setState({input: input, showWeek: false, detailIndex: ""}) : this.setState({input: input, selected: "current", showWeek: false, detailIndex: ""});
      if (this.state.historicOpen) {
        this._getTimestampHistory();
      }
      this._getCoordinates(input);
    }
  }

  _getCoordinates(input) {
    var geocoder = new google.maps.Geocoder();
    geocoder.geocode({"address": input}, function(results, status) {
          if (status == google.maps.GeocoderStatus.OK) {
            var lat = results[0].geometry.location.lat();
            var lon = results[0].geometry.location.lng();
            this.setState({
              coords: [lat, lon],
              input: input,
              initial: false
            });
            this._findWeather([lat, lon]);
          }
          else {
            alert("The Geocode was not successful for the following reason: " + status);
          }
        }.bind(this)
      );
  }

  _findWeather(coords) {
    const BASEURL = "https://api.forecast.io/forecast/3a819ed8d6456eb83e53691316d696e8/";
    var coordinates = coords[0] + "," + coords[1];
    var url = this.state.imperialUnits ? (BASEURL + coordinates) : (BASEURL + coordinates + "?&units=si");
    $.ajax({
      url: url,
      dataType: "jsonp",
      type: "GET"
    }).success(function(data) {
        if (this.refs.modal) { this.refs.modal.hide(); }
        this.setState({weather: data, loadingLocation: false});
        this._handleData(data);
      }.bind(this));
  }

  _handleData(data) {
    var historyStore = this.state.history;
    var place = this.state.input;
    var weatherData = {
      "place": place,
      "lat": data.latitude,
      "lon": data.longitude,
    };
    if (historyStore.length <= 10) {
      if (historyStore.length < 1 || (historyStore.length >= 1 && weatherData.place != historyStore[0].place)) {
        historyStore.unshift(weatherData);
        this.setState({history: historyStore});
      }
    }
    else {
      if (weatherData.place != historyStore[0].place) {
        historyStore.unshift(weatherData);
        historyStore.pop();
        this.setState({history: historyStore});
      }
    }
  }

  _onOptionSelect(e) {
    var value = e.target.value;
    switch(value) {
      case "current":
        this.setState({selected: "current"});
        break;
      case "searched":
        this.setState({selected: "searched"});
        break;
      default:
        this.setState({selected: "current"});
        break;
    }
  }

  _onHistoryItemClick(e) {
    var input = e.target.value;
    this.setState({input: input, selected: "current", showWeek: false, detailIndex: ""}, function() {
      this._getCoordinates(input);
      if (this.state.historicOpen) {
        this._getTimestampHistory();
      }
    }.bind(this));
  }

  _showWeek() {
    if (this.state.showWeek) {
      this.setState({detailIndex: ""});
    }
    this.setState({showWeek: !this.state.showWeek})
  }

  _showMoreDetail(e) {
    var index = e.currentTarget.value;
    if (index === this.state.detailIndex) {
      this.setState({detailIndex: ""});
    }
    else {
      this.setState({detailIndex: e.currentTarget.value});
    }
  }

  _getTimestamp() {
    var current = new Date(this.state.weather.currently.time * 1000);
    var month = (current.getMonth() + 1).toString();
    var day = current.getDate().toString();
    var year = current.getFullYear();
    var hour = current.getHours().toString();
    var minute = current.getMinutes().toString();
    var second = current.getSeconds().toString();

    var month = month.length < 2 ? "0" + month : month;
    var day = day.length < 2 ? "0" + day : day;
    var hour = hour.length < 2 ? "0" + hour : hour;
    var minute = minute.length < 2 ? "0" + minute : minute;
    var second = second.length < 2 ? "0" + second : second;

    var yearPlusTimestamp = [year, ("-" + month + "-" + day + "T" + hour + ":" + minute +  ":" + second)];

    return yearPlusTimestamp;
  }

  _getTimestampHistory() {
    this.setState({timestampHistory: []});
    if (this.state.historicOpen && !this.state.unitChange) {
      $(".graph").css({
         "display" : "none",
         "width" : "0px",
         "height" : "0px"
      });
      this.setState({historicOpen: false});
    }
    else {
      var yearPlusTimestamp = this._getTimestamp();
      var currentYear = yearPlusTimestamp[0];
      var timestampNoYear = yearPlusTimestamp[1];
      var timestampArray = [];
      var yearCounter = 0;

      while (yearCounter < 10) {
      timestampArray.push(currentYear + timestampNoYear);
      currentYear -= 1;
      yearCounter+= 1;
      }

      for (var i = 0; i < timestampArray.length; i++) {
        const BASEURL = "https://api.forecast.io/forecast/3a819ed8d6456eb83e53691316d696e8/";
        var coordinates = this.state.coords[0] + "," + this.state.coords[1] + ",";
        var url = this.state.imperialUnits ? (BASEURL + coordinates + timestampArray[i]) : (BASEURL + coordinates + timestampArray[i] + "?&units=si");
        $.ajax({
          url: url,
          dataType: "jsonp",
          type: "GET"
        }).success(function(data) {
          var history = this.state.timestampHistory;
            history.unshift(data);
            history.sort(function(a, b) {
              var aYear = (new Date(a.currently.time * 1000)).getFullYear();
              var bYear = (new Date(b.currently.time * 1000)).getFullYear();
              return aYear - bYear;
            });
            this._getGraph();
            this.setState({timestampHistory: history});
          }.bind(this));
      }
      this.setState({historicOpen: true, unitChange: false});
    }
  }

  _getGraph() {
    var currentDate = this._getCurrentDate(this.state.weather.currently.time);
    var history = this.state.timestampHistory;
    var years = [];
    var currentTemps = [];
    var dailyMaxTemps = [];
    var dailyMinTemps = [];
    $.each(history, function(index, item) {
      var daily = item.daily.data[0];
      years.push((new Date(item.currently.time * 1000)).getFullYear());
      currentTemps.push(Math.round(item.currently.temperature));
      dailyMinTemps.push(Math.round(daily.temperatureMin));
      dailyMaxTemps.push(Math.round(daily.temperatureMax));
    });
      var graphOptions =  {
        chart: {
              type: "line"
          },
          title: {
              text: currentDate + " Temperatures"
          },
          subtitle: {
            text: "Past 10 Years"
          },
          xAxis: {
              categories: years,
              title: {
                text: "Years"
              }
          },
          yAxis: {
              title: {
                  text: "Temperature (°)"
              }
          },
          plotOptions: {
              line: {
                  dataLabels: {
                      enabled: true
                  },
                  enableMouseTracking: true
              }
          },
        series: [{
            name: "Actual Temperature",
            data: currentTemps,
            color: "#5342F0"
        }, {
            name: "Daily Min. Temperature",
            data: dailyMinTemps,
            color: "#7E379C"
        }, {
            name: "Daily Max. Temperature",
            data: dailyMaxTemps,
            color: "#C12B70"
        }]
      }
      $(".graph").highcharts(graphOptions);
      $(".graph").css({"display" : "block", "width" : "95%", "height":"500px"});
  }

  _getCurrentDate(timestamp) {
    var monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    var date = new Date(timestamp * 1000);
    var month = date.getMonth() + 1;
    var day = date.getDate();
    return monthNames[month] + " " + day;
  }

  render() {
    var modalStyles = {
      height: "145px",
      width: "350px",
    };
    var loading = this.state.loadingLocation ? (<div className="spinner"></div>) : <div />;

    if (!$.isEmptyObject(this.state.weather) && this.state.history.length > 0) {
      var content;
      switch(this.state.selected) {
        case "current":
          content = <CurrentWeather currentWeather={this.state.weather} imperialUnits={this.state.imperialUnits} address={this.state.input} onShowWeek={this._showWeek.bind(this)} showWeek={this.state.showWeek} showMoreDetail={this._showMoreDetail.bind(this)} detailIndex={this.state.detailIndex} getTimestampHistory={this._getTimestampHistory.bind(this)} />;
          break;
        case "searched":
          content = <HistoryList history={this.state.history} onHistoryItemClick={this._onHistoryItemClick.bind(this)} />;
          break;
      }

      return (
        <div>
          <h1 className="title">SkyCast</h1>
          <SkyLight className="modal" dialogStyles={modalStyles} ref="modal">
            <Permission geolocate={this._geolocate.bind(this)} denyGeolocation={this._denyGeolocation.bind(this)} />
            {loading}
          </ SkyLight>
          <Search onChange={this._onChangeSearchText.bind(this)} onPlaceSearch={this._onPlaceSearch.bind(this)}  showPermission={this._showPermission.bind(this)} imperialUnits={this.state.imperialUnits} changeUnit={this._changeUnit.bind(this)} />
          <OptionsBar onOptionSelect={this._onOptionSelect.bind(this)} />
          {content}
        </div>
      );
    }
    else {
      return (
        <div />
      );
    }
  }

}

ReactDOM.render(<App />, document.querySelector(".container"));